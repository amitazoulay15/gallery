#pragma once
#include "sqlite3.h"
#include <iostream>
#include <io.h>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include "Album.h"
#include "User.h"
#include "IDataAccess.h"

class galleryDatabase : public IDataAccess
{
public:
	galleryDatabase() = default;
	virtual ~galleryDatabase() = default;
	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album& pAlbum) override;
	void printAlbums() override;

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	User getUser(int userId) override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;


	// user statistics
	int countAlbumsOwnedOfUser(const User& user);
	int countAlbumsTaggedOfUser(const User& user);
	int countTagsOfUser(const User& user);
	float averageTagsPerAlbumOfUser(const User& user);

	// queries
	User getTopTaggedUser();
	Picture getTopTaggedPicture();
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	bool open() override;
	void close() override;
	void clear() override;


};

