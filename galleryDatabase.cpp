#include "galleryDatabase.h"



/*global var*/
std::list<Picture> picPtr;
std::list<Picture> picPtr2;
bool flag = false;
sqlite3* db;
std::list<Album> ptr;

/*callback*/
int callback_int(void* data, int argc, char** argv, char** azColName)
{
	int* dataPtr = (int*)data;
	int num;
	std::stringstream geek(argv[argc - 1]);
	geek >> num;
	*dataPtr = num;
	return 0;
}
int callback_string(void* data, int argc, char** argv, char** azColName)
{
	std::string* ptr = (std::string*) data;
	*ptr = argv[argc - 1];
	return 0;
}
int callback_print(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << "=" << argv[i] << "|";
	}
	std::cout << std::endl;
	return 0;
}
int callback_checkAlbum(void* data, int argc, char** argv, char** azColName)
{
	std::string* dataPtr = (std::string*)data;
	for (int i = 0; i < argc; i++)
	{
		if (*dataPtr == argv[i])
		{
			flag = true;
			return 0;
		}
	}
	return 0;
}
int callback_checkUser(void* data, int argc, char** argv, char** azColName)
{
	int* d = (int*)data;
	int g = *d;
	for (int i = 0; i < argc; i++)
	{
		std::string s = std::to_string(g);
		if (s == argv[i])
		{
			flag = true;
		}
	}
	return 0;
}
int callback_getAlbumOfuser(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>* album = (std::list<Album>*)data;
	int num;
	std::stringstream geek(argv[argc - 1]);
	geek >> num;
	Album A(num, argv[1]);
	ptr.push_back(A);
	*album = ptr;
	return 0;
}
int callback_pictures(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* pic = (std::list<Picture>*)data;
	int num;
	std::stringstream geek(argv[argc - 1]);
	geek >> num;
	Picture A(num, argv[0]);

	picPtr.push_back(A);
	*pic = picPtr;
	return 0;
}
int callback_gettagofpic(void* data, int argc, char** argv, char** azColName)
{
	std::vector<int>* dataPtr = (std::vector<int>*)data;
	int num;
	std::stringstream geek(argv[argc - 1]);
	geek >> num;
	dataPtr->push_back(num);
	return 0;
}
int callback_openAlbum(void* data, int argc, char** argv, char** azColName)
{
	char* errMessage = nullptr;
	std::string name;
	int userid; int num;
	std::list<Picture>* pic = (std::list<Picture>*)data;
	std::vector<int> vec;


	std::stringstream geek(argv[argc - 1]);
	geek >> num;

	std::string msg("SELECT USER_ID FROM TAGS WHERE PICTURE_ID = " + std::to_string(num) + "; ");
	sqlite3_exec(db, msg.c_str(), callback_gettagofpic, &vec, &errMessage);
	Picture A(num, argv[1]);
	A.setPath(argv[2]);
	for (int i = 0; i < vec.size(); i++)
	{
		A.tagUser(vec[i]);
	}
	picPtr2.push_back(A);
	*pic = picPtr2;
	return 0;
}
int callback_countAlbumsTaggedOfUser(void* data, int argc, char** argv, char** azColName)
{
	std::map<int, std::vector<int>>* ptrData = (std::map<int, std::vector<int>>*)data;
	int albumId = atoi(argv[7]);
	int userId = atoi(argv[2]);

	if (ptrData->count(userId) == 0)
	{
		ptrData->insert(std::pair<int, std::vector<int>>(userId, std::vector<int>()));
	}
	if (std::find(ptrData->at(userId).begin(), ptrData->at(userId).end(), albumId) == ptrData->at(userId).end())
	{
		ptrData->insert(std::pair<int, std::vector<int>>(userId, std::vector<int>()));
	}
	return 0;
}
/****methodes****/

/*This methode open the database
input : void - There is not input to the function
return : res - The resulut of the opening , if the database was opened the methode return treu else the method return false
rtype : bool*/
bool galleryDatabase::open()
{
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &(db));
	if (res != SQLITE_OK)
		std::cout << "error3";
	return res;
}
/*
This methode close the database
input : void - There is not input to the function
return : void
*/
void galleryDatabase::close()
{
	sqlite3_close(db);
	db = nullptr;
}
/*
This function clear the database , drop all the tables of it.
input : void
return : void
*/
void clear()
{
	char* errMessage = nullptr;
	int res = 0;

	const char* sqlStatement = "DROP TABLE IF EXISTS USERS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS ALBUMS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS PICTURES;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS TAGS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	picPtr.clear();
	picPtr2.clear();
	ptr.clear();
}
void clear1()
{
	char* errMessage = nullptr;
	int res = 0;

	const char* sqlStatement = "DROP TABLE IF EXISTS USERS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS ALBUMS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS PICTURES;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS TAGS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	picPtr.clear();
	picPtr2.clear();
	ptr.clear();
}
void galleryDatabase::closeAlbum(Album& pAlbum) {}
void galleryDatabase::deleteAlbum(const std::string& albumName, int userId)
{
	std::string sqlStatement = "DELETE FROM ALBUMS WHERE NAME='" + albumName + "'" + ';';
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	std::string albumQuery("(SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "'" + "LIMIT 1 OFFSET 0)");
	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_id=" + albumQuery + ';';

	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
}
bool galleryDatabase::doesAlbumExists(const std::string& albumName, int userId)
{

	flag = false;
	char* errMessage = nullptr;

	std::string albumList = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(userId) + ";";
	std::string nameOfAlbum = albumName;
	sqlite3_exec(db, albumList.c_str(), callback_checkAlbum, &nameOfAlbum, &errMessage);

	if (flag)
	{
		return true;
	}

	return false;

}

void galleryDatabase::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{

	char* errMessage = nullptr;
	std::string albumQuery("(SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "'" + "LIMIT 1 OFFSET 0)");
	std::string addPicture("INSERT INTO PICTURES (NAME,LOCATION,CREATION_DATE,ALBUM_ID) VALUES('" + std::string(picture.getName()) + std::string("', '") + std::string(picture.getPath()) + std::string("', '") + std::string(picture.getCreationDate()) + std::string("', ") + albumQuery + std::string(");"));

	sqlite3_exec(db, addPicture.c_str(), nullptr, nullptr, &errMessage);

}
void galleryDatabase::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	char* errMessage = nullptr;
	std::string AlbumId(std::string("(SELECT ID FROM ALBUMS WHERE NAME ='") + albumName + std::string("')"));
	std::string pictureId(std::string("(SELECT ID FROM PICTURES WHERE NAME = '") + pictureName + std::string("')"));

	std::string removePicture = "DELETE FROM PICTURES WHERE ID = " + pictureId + "AND ALBUM_ID = " + AlbumId + ";";
	std::cout << removePicture;
	std::string removeTag = "DELETE FROM TAGS WHERE PICTURE_ID = " + pictureId + ";";

	sqlite3_exec(db, removePicture.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(db, removeTag.c_str(), nullptr, nullptr, &errMessage);
}
void  galleryDatabase::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMessage = nullptr;

	std::string str = "INSERT INTO TAGS(picture_id, user_id) VALUES((SELECT id FROM PICTURES WHERE name='" + pictureName + "' AND album_id=(SELECT id FROM ALBUMS WHERE name='" + albumName + "'))," + std::to_string(userId) + ");";
	sqlite3_exec(db, str.c_str(), nullptr, nullptr, &errMessage);
}
void galleryDatabase::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMessage = nullptr;
	std::string AlbumId(std::string("(SELECT ID FROM ALBUMS WHERE NAME ='") + albumName + std::string("' LIMIT 1 OFFSET 0)"));
	std::string pictureId(std::string("(SELECT ID FROM PICTURES WHERE ALBUM_ID =") + AlbumId + "AND NAME = '" + pictureName + std::string("' OFFSET 0)"));

	std::string removeTag = "DELETE FROM TAGS WHERE PICTURE_ID = " + pictureId + "AND USER_ID = " + std::to_string(userId) + ';';

	sqlite3_exec(db, removeTag.c_str(), nullptr, nullptr, &errMessage);
}
void galleryDatabase::createUser(User& user)
{
	char* errMessage = nullptr;
	std::string addUserToUsers(std::string("INSERT INTO USERS (NAME, ID) VALUES(") + std::string("'") + user.getName() + std::string("', ") + std::string(std::to_string(user.getId())) + std::string(");"));

	sqlite3_exec(db, addUserToUsers.c_str(), nullptr, nullptr, &errMessage);

}
void galleryDatabase::deleteUser(const User& user)
{
	char* errMessage = nullptr;

	std::string delUser(std::string("DELETE FROM USERS WHERE ID =") + std::string(std::to_string(user.getId())) + ";");
	std::string delTagOfUser("DELETE FROM TAGS WHERE USER_ID = " + std::string(std::to_string(user.getId())) + ";");
	std::string getAlbumId("(SELECT ID FROM ALBUMS WHERE USER_ID = " + std::string(std::to_string(user.getId())) + ")");
	std::string delAlbumOfuser("DELETE FROM ALBUMS WHERE USER_ID = " + std::string(std::to_string(user.getId())) + ";");
	std::cout << delAlbumOfuser;
	std::string delPicOfuser("DELETE FROM PICTURES WHERE ALBUM_ID = " + getAlbumId + ";");

	sqlite3_exec(db, delTagOfUser.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(db, delPicOfuser.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(db, delAlbumOfuser.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(db, delUser.c_str(), nullptr, nullptr, &errMessage);

}

bool galleryDatabase::doesUserExists(int userId)
{

	flag = false;
	char* errMessage = nullptr;
	std::string userQuery("SELECT * FROM USERS;");

	sqlite3_exec(db, userQuery.c_str(), callback_checkUser, &userId, &errMessage);

	if (flag)
	{
		return true;
	}
	else { return false; }
}

void galleryDatabase::printAlbums()
{
	char* errMessage = nullptr;
	std::string userId("SELECT * FROM ALBUMS;");

	sqlite3_exec(db, userId.c_str(), callback_print, nullptr, &errMessage);
}
void galleryDatabase::printUsers()
{
	char* errMessage = nullptr;
	std::string userId("SELECT * FROM USERS;");

	sqlite3_exec(db, userId.c_str(), callback_print, nullptr, &errMessage);
}
User galleryDatabase::getUser(int userId)
{
	std::string name;
	char* errMessage = nullptr;
	std::string Username("SELECT NAME FROM USERS WHERE ID = " + std::string(std::to_string(userId)));
	sqlite3_exec(db, Username.c_str(), callback_string, &name, &errMessage);
	return User(userId, name);
}

void galleryDatabase::createAlbum(const Album& album)
{
	char* errMessage = nullptr;
	std::string userId("INSERT INTO ALBUMS (NAME,CREATION_DATE,USER_ID) VALUES('" + album.getName() + "','" + album.getCreationDate() + "'," + std::string(std::to_string(album.getOwnerId())) + ");");

	sqlite3_exec(db, userId.c_str(), nullptr, nullptr, &errMessage);
}

int galleryDatabase::countAlbumsOwnedOfUser(const User& user)
{
	int cnt = 0;
	char* errMessage = nullptr;
	std::string cntAlbumOwnedOfUser("SELECT COUNT(ID) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()));
	sqlite3_exec(db, cntAlbumOwnedOfUser.c_str(), callback_int, &cnt, &errMessage);

	return cnt;
}
const std::list<Album> galleryDatabase::getAlbums()
{
	char* errMessage = nullptr;
	std::string getAlbums("SELECT * FROM ALBUMS;");
	std::list<Album> a;
	int res = sqlite3_exec(db, getAlbums.c_str(), callback_getAlbumOfuser, &a, &errMessage);
	ptr.clear();
	return a;
}
const std::list<Album> galleryDatabase::getAlbumsOfUser(const User& user)
{
	char* errMessage = nullptr;
	std::string getAlbums("SELECT * FROM ALBUMS WHERE USER_ID = " + std::string(std::to_string(user.getId())));
	std::list<Album> a;
	int res = sqlite3_exec(db, getAlbums.c_str(), callback_getAlbumOfuser, &a, &errMessage);
	ptr.clear();
	return a;
}
Album galleryDatabase::openAlbum(const std::string& albumName)
{
	clear1();
	int id;
	galleryDatabase call;
	std::list<Picture> b;
	std::list <Picture> ::iterator it;
	char* errMessage = nullptr;
	std::string msg("SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "';");
	int res = sqlite3_exec(db, msg.c_str(), callback_int, &id, &errMessage);
	std::string msg2("SELECT ID,NAME,LOCATION FROM PICTURES WHERE ALBUM_ID = " + std::to_string(id) + ";");
	sqlite3_exec(db, msg2.c_str(), callback_openAlbum, &b, &errMessage);
	Album a(id, albumName);
	for (it = b.begin(); it != b.end(); ++it)
	{
		a.addPicture(*it);
	}

	ptr.clear();
	picPtr.clear();
	picPtr2.clear();
	return a;
}
int galleryDatabase::countTagsOfUser(const User& user)
{
	char* errMessage = nullptr;
	int cnt;
	std::string cntTag("SELECT COUNT(USER_ID) FROM TAGS WHERE USER_ID =" + std::string(std::to_string(user.getId())));
	int res = sqlite3_exec(db, cntTag.c_str(), callback_int, &cnt, &errMessage);
	return cnt;
}
int galleryDatabase::countAlbumsTaggedOfUser(const User& user)
{
	std::string query("SELECT* FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID INNER JOIN ALBUMS ON ALBUMS.ID = PICTURES.ALBUM_ID GROUP BY ALBUMS.ID;");
	char* errMessage = nullptr;
	std::map<int, std::vector<int>> myMap;
	int tags = 0;

	sqlite3_exec(db, query.c_str(), callback_countAlbumsTaggedOfUser, &myMap, &errMessage);

	if (myMap.count(user.getId()) > 0)
	{
		tags = myMap[user.getId()].size();
	}

	return tags;
}
float galleryDatabase::averageTagsPerAlbumOfUser(const User& user)
{
	char* errMessage = nullptr;
	int cntUseridInTag;
	int cntUseridInAlbum;
	float avg;
	std::string name;
	std::string a("SELECT COUNT(USER_ID) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";");
	sqlite3_exec(db, a.c_str(), callback_int, &cntUseridInTag, &errMessage);
	std::string b("SELECT COUNT(USER_ID) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";");
	std::cout << b;
	sqlite3_exec(db, b.c_str(), callback_int, &cntUseridInAlbum, &errMessage);



	avg = (float)cntUseridInTag / (float)cntUseridInAlbum;
	return avg;
}

User galleryDatabase::getTopTaggedUser()
{
	char* errMessage = nullptr;
	int userid;
	std::string name;
	std::string a("SELECT USER_ID FROM TAGS GROUP BY USER_ID ORDER BY COUNT(*) DESC LIMIT 1;");
	sqlite3_exec(db, a.c_str(), callback_int, &userid, &errMessage);
	std::string b("SELECT NAME FROM USERS WHERE ID = " + std::to_string(userid) + ";");
	sqlite3_exec(db, b.c_str(), callback_string, &name, &errMessage);
	User user(userid, name);
	return user;
}

Picture galleryDatabase::getTopTaggedPicture()
{
	char* errMessage = nullptr;
	int userid, picId;
	std::string name;
	std::string a("SELECT PICTURE_ID FROM TAGS GROUP BY PICTURE_ID ORDER BY COUNT(*) DESC LIMIT 1;");
	sqlite3_exec(db, a.c_str(), callback_int, &userid, &errMessage);
	std::string b("SELECT PICTURE_ID FROM TAGS WHERE USER_ID =" + std::to_string(userid) + ";");
	sqlite3_exec(db, b.c_str(), callback_int, &picId, &errMessage);
	std::string c("SELECT NAME FROM PICTURES WHERE ID = " + std::to_string(picId) + ";");
	sqlite3_exec(db, c.c_str(), callback_string, &name, &errMessage);
	Picture pic(picId, name);
	return pic;
}

std::list<Picture> galleryDatabase::getTaggedPicturesOfUser(const User& user)
{
	char* errMessage = nullptr;
	std::list<Picture> a;
	std::string getQuery("SELECT ID,NAME FROM PICTURES INNER JOIN TAGS ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID = " + std::to_string(user.getId()) + ";");
	sqlite3_exec(db, getQuery.c_str(), callback_pictures, &a, &errMessage);

	return a;
}
