#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
#include "MemoryAccess.h"
#include "AlbumManager.h"
#include "galleryDatabase.h"
#include "IDataAccess.h"

/*
This function return the number command that the user choose
input : void
return std::atoi(input.c_str()) :   the number command that the user choose
rtype : int
*/
int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");

	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);

	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}

	return std::atoi(input.c_str());
}
/*
This function print the shystem detail of the gallery
input : void
return : void
*/
void printingSystemDetails()
{
	//std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "Project developer is : Amit Azoulay \n";
	//std::cout << "curnnet time and date : " << std::ctime(&end_time);
	std::cout << "===================" << std::endl;
}


int main(void)
{

	printingSystemDetails();
	// initialization data access
	galleryDatabase dataAccess;

	// initialize album manager
	AlbumManager albumManager(dataAccess);


	std::string albumName;
	std::cout << "Welcome to Gallery!" << std::endl;
	std::cout << "===================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;

	do {
		int commandNumber = getCommandNumberFromUser();

		try {
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		}
		catch (std::exception & e) {
			std::cout << e.what() << std::endl;
		}
	} while (true);
}


